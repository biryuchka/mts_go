package main

import (
	"errors"
	"fmt"
)

func Hash1(dataStr *string) uint64 {
	data := []byte(*dataStr)
	var hash uint64
	for _, b := range data {
		hash = uint64(b) + (hash << 6) + (hash << 16) - hash
	}
	return hash
}
func Hash2(dataStr *string) uint64 {
	data := []byte(*dataStr)
	hash := uint64(5381)

	for _, b := range data {
		hash += uint64(b) + hash + hash<<5
	}

	return hash
}

type Book struct {
	title  string
	author string
}

// получение атрибутов
func (book *Book) GetTitle() string {
	return book.title
}
func (book *Book) GetAuthor() string {
	return book.author
}

// задание атрибутов
func (book *Book) NewTitle(title_ string) {
	book.title = title_
}
func (book *Book) NewAuthor(author_ string) {
	book.author = author_
}

func (book *Book) PrintBook(err bool, lib string) {
	if err {
		fmt.Println("Такой книги нет в", lib, " библиотеке")
	} else {
		fmt.Println("Запрос о книге: автор - ", book.author, " и название -", book.title)
	}
}

type Storage interface {
	GetByID(id uint64) (Book, error)
	AddBook(id uint64, book *Book) error
}

type MapStorage struct {
	rep map[uint64]*Book
}
type SliceStorage struct {
	rep []*Book
}

func (storage *MapStorage) GetByID(id uint64) (Book, error) {
	_, ok := storage.rep[id]
	if ok {
		return *storage.rep[id], nil
	}
	return Book{}, errors.New("элемента не существует")
}
func (storage *MapStorage) AddBook(id uint64, book *Book) error {
	_, ok := storage.rep[id]
	if !ok {
		storage.rep[id] = book
		return nil
	}
	return errors.New("элемент с таким же id уже существует")
}

func (storage *SliceStorage) GetByID(id uint64) (Book, error) {
	if id < uint64(len(storage.rep)) && id >= 0 {
		return *storage.rep[id], nil
	}
	return Book{}, errors.New("элемента не существует")
}
func (storage *SliceStorage) AddBook(id uint64, book *Book) error {
	if id == uint64(len(storage.rep)) {
		// fmt.Println(len(storage.rep))
		storage.rep = append(storage.rep, book)
		return nil
	}
	return errors.New("id не подходит")
}

type Library struct {
	storage    Storage
	ids        map[uint64]uint64
	generateId func(str *string) uint64
}

func (library *Library) AddBook(book Book) error {
	title := book.GetTitle()
	id := library.generateId(&title)
	idRep := id
	err := library.storage.AddBook(idRep, &book)
	if err != nil {
		idRep = uint64(len(library.ids))
		err = library.storage.AddBook(idRep, &book)
		if err != nil {
			return err
		}
	}
	library.ids[id] = idRep
	return nil
}

func (library *Library) GetByName(name string) (Book, error) {
	title := library.generateId(&name)
	_, ok := library.ids[title]
	if !ok {
		return Book{}, errors.New("элемента не существует")
	}
	id := library.ids[library.generateId(&name)]
	return library.storage.GetByID(id)
}

func main() {
	books := []Book{
		{title: "book1", author: "author1"},
		{title: "book2", author: "author2"},
		{title: "book3", author: "author3"},
		{title: "book4", author: "author4"},
		{title: "book5", author: "author5"},
	}
	libraryMap := Library{
		storage:    &MapStorage{make(map[uint64]*Book)},
		ids:        make(map[uint64]uint64),
		generateId: Hash1,
	}
	librarySlice := Library{
		storage:    &SliceStorage{make([]*Book, 0)},
		ids:        make(map[uint64]uint64),
		generateId: Hash2,
	}

	for _, element := range books {
		err1 := libraryMap.AddBook(element)
		if err1 != nil {
			panic(err1)
		}
		err2 := librarySlice.AddBook(element)
		if err2 != nil {
			panic(err2)
		}
	}

	requests := []string{
		"book3",
		"book1",
		"book7",
	}

	for _, i := range requests {
		book, err := libraryMap.GetByName(i)
		book.PrintBook(err != nil, "мап")
	}
	for _, i := range requests {
		book, err := librarySlice.GetByName(i)
		book.PrintBook(err != nil, "слайс")
	}
}

package main

import (
	"context"
	"flag"
	"fmt"
	"homework2/internal/client"
	"homework2/internal/config"
	"os"
)

func main() {
	var filepath string
	flag.StringVar(&filepath, "c", ".config.yaml", "set config path")
	flag.Parse()

	cfg, err := config.NewConfig(filepath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	clientApp := client.NewClient(cfg)
	ctx := context.Background()

	res1, err := clientApp.CheckGetVersion()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res1)
	}

	res2, err := clientApp.CheckGetDecode("SGVsbG8h")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res2)
	}

	res3, err := clientApp.CheckPostHardOP(ctx)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res3)
	}

}

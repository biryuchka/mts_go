package handlers

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"math/rand"
	"net/http"
	"time"

	"homework2/internal/config"
)

func DecodingString64(str string) (string, error) {
	decodeStr, err := base64.StdEncoding.DecodeString(str)
	return string(decodeStr), err
}

type Handler struct {
	cfg *config.Config
}

func NewHandler(config *config.Config) *Handler {
	return &Handler{cfg: config}
}

func (handler *Handler) GetVersion(writer http.ResponseWriter, _ *http.Request) {
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write([]byte(handler.cfg.Version))
}

func (handler *Handler) PostDecode(writer http.ResponseWriter, request *http.Request) {
	type InputString struct {
		Input string `json:"inputString"`
	}
	type OutputString struct {
		Output string `json:"outputString"`
	}
	body, err := io.ReadAll(request.Body)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func() {
		err := request.Body.Close()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}()
	var inputStr InputString
	err = json.Unmarshal(body, &inputStr)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	var outputStr OutputString
	tmp, err := DecodingString64(inputStr.Input)
	outputStr.Output = tmp
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}

	ans, err := json.Marshal(outputStr)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, _ = writer.Write(ans)
	writer.WriteHeader(http.StatusOK)
}

func (handler *Handler) GetHardOP(writer http.ResponseWriter, _ *http.Request) {
	time.Sleep(time.Duration(10+rand.Intn(10+1)) * time.Second)
	switch rand.Intn(2) {
	case 0:
		writer.WriteHeader(http.StatusGatewayTimeout)
	case 1:
		writer.WriteHeader(http.StatusOK)
	}
}

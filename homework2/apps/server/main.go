package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"homework2/internal/config"
	"homework2/internal/server"
)

func main() {
	var filepath string
	flag.StringVar(&filepath, "c", ".config.yaml", "set config path")
	flag.Parse()

	cfg, err := config.NewConfig(filepath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	serverApp := server.NewServer(cfg)
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	defer func() {
		v := recover()
		if v != nil {
			ctx, _ := context.WithTimeout(ctx, 3*time.Second)
			serverApp.Stop(ctx)
			fmt.Println(v)
			os.Exit(1)
		}

	}()

	serverApp.Run()

	<-ctx.Done()
	ctx, _ = context.WithTimeout(ctx, 3*time.Second)
	serverApp.Stop(ctx)
}

package client

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"time"

	"homework2/internal/config"
)

type Client struct {
	cfg *config.Config
}

func NewClient(config *config.Config) *Client {
	return &Client{cfg: config}
}

type InputString struct {
	Input string `json:"inputString"`
}
type OutputString struct {
	Output string `json:"outputString"`
}

func (c *Client) CheckGetVersion() (string, error) {
	url := "http://localhost:8080/version"
	response, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)
	body, err := httputil.DumpResponse(response, true)
	if err != nil {
		log.Fatalln(err)
	}
	return string(body), nil
}

func (c *Client) CheckGetDecode(str string) (string, error) {
	url := "http://localhost:8080/decode"
	input, err := json.Marshal(InputString{Input: str})
	if err != nil {
		return "", err
	}
	response, err := http.Post(url, "application/json", bytes.NewBuffer(input))
	if err != nil {
		return "", err
	}
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(response.Body)
	var result OutputString
	err = json.Unmarshal(body, &result)
	if err != nil {
		return "", err
	}
	return result.Output, nil
}

func (c *Client) CheckPostHardOP(ctx context.Context) (int, error) {
	url := "http://localhost:8080/hard-op"
	ctx, stop := context.WithTimeout(ctx, 13*time.Second)
	defer stop()
	err := make(chan error)
	var resp_ http.Response
	go func() {
		temp, err_ := http.Get(url)
		resp_ = *temp
		if err != nil {
			err <- err_
		} else {
			err = nil
		}
	}()
	select {
	case err_ := <-err:
		if err_ != nil {
			return 0, err_
		} else {
			return resp_.StatusCode, nil
		}
	case <-ctx.Done():
		return 0, fmt.Errorf("timeout")
	}
}

package server

import (
	"context"
	"fmt"
	"homework2/apps/server/handlers"
	"homework2/internal/config"
	"net/http"
)

type Server struct {
	cfg    *config.Config
	server *http.Server
}

func NewServer(cfg *config.Config) *Server {
	addr := fmt.Sprintf(":%d", cfg.Http.Port)
	return &Server{
		cfg: cfg,
		server: &http.Server{
			Addr:    addr,
			Handler: initApi(cfg),
		},
	}
}

func (s *Server) Run() {
	go func() {
		err := s.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func (s *Server) Stop(ctx context.Context) {
	fmt.Println(s.server.Shutdown(ctx))
}

func initApi(cfg *config.Config) http.Handler {
	mux := http.NewServeMux()
	handler := handlers.NewHandler(cfg)
	mux.HandleFunc("/version", handler.GetVersion)
	mux.HandleFunc("/decode", handler.PostDecode)
	mux.HandleFunc("/hard-op", handler.GetHardOP)
	return mux
}

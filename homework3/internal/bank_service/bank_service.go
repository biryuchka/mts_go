package bank_service

import "homework3/internal/database"

type BankService struct {
	db *database.DataBase
}

func NewBankService(db *database.DataBase) *BankService {
	return &BankService{db: db}
}

func (bank *BankService) BankTransaction(amount int, from int, to int) (int, int, string) {
	user_from := (*bank.db).FindUser(from)
	user_to := (*bank.db).FindUser(to)
	str_err := ""
	if amount <= 0 {
		str_err = "Сумма - число положительное"
	}
	if from == to {
		str_err = "Перевод самому себе - запрещен"
	}
	if user_from == nil || user_to == nil {
		str_err = "Пользователь не найден"
		return -1, -1, str_err
	}
	if user_from.GetBalance() < amount {
		str_err = "Недостаточно средств"
	}

	if str_err == "" {
		user_to.SetBalance((user_to).GetBalance() + amount)
		user_from.SetBalance((user_to).GetBalance() - amount)
	}
	return user_to.GetId(), user_to.GetBalance(), str_err
}

func (bank *BankService) GetBalance(id int) (int, int, string) {
	user_ := (*bank.db).FindUser(id)
	str_err := ""
	if user_ == nil {
		str_err = "Пользователь не найден"
		return -1, -1, str_err
	}
	return user_.GetId(), user_.GetBalance(), str_err
}

func (bank *BankService) PutMoneyBalance(id int, amount int) (int, int, string) {
	user_ := (*bank.db).FindUser(id)
	str_err := ""
	if amount <= 0 {
		str_err = "Сумма - число положительное"
	}
	if user_ == nil {
		str_err = "Пользователь не найден"
		return -1, -1, str_err
	}
	if str_err == "" {
		user_.SetBalance((user_).GetBalance() + amount)
	}
	return user_.GetId(), user_.GetBalance(), str_err
}

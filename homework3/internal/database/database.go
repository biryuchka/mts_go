package database

type DataBase interface {
	FindUser(from int) User
}

package my

import (
	"homework3/internal/database"
)

type DataBase struct {
	Users []User
}

func NewDataBase() *DataBase {
	users_ := make([]User, 0)
	users_ = append(users_, *NewUser(0, 1000))
	users_ = append(users_, *NewUser(1, 30))
	users_ = append(users_, *NewUser(2, 330))
	return &DataBase{Users: users_}
}

func (db *DataBase) FindUser(from int) database.User {
	for _, user := range db.Users {
		if user.GetId() == from {
			return &user
		}
	}
	return nil
}

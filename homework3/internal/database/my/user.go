package my

type User struct {
	id      int
	balance int
}

func NewUser(id_ int, balance_ int) *User {
	return &User{id: id_, balance: balance_}
}

func (user *User) GetId() int {
	return user.id
}

func (user *User) GetBalance() int {
	return user.balance
}

func (user *User) SetBalance(new_balance int) {
	user.balance = new_balance
}

package database

type User interface {
	GetId() int
	SetBalance(new_balance int)
	GetBalance() int
}

package model

type Transaction struct {
	Amount int `json:"amount"`
	From   int `json:"user_from"`
	To     int `json:"user_to"`
}

type RequestGet struct {
	UserId int `json:"user_id"`
}

type RequestPut struct {
	UserId int `json:"user_id"`
	Amount int `json:"amount"`
}

type Response struct {
	UserId  int `json:"user_id"`
	Balance int `json:"balance"`
}

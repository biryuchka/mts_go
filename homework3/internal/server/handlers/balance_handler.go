package handlers

import (
	"encoding/json"
	"homework3/internal/model"
	"io"
	"net/http"
)

func (handler *Handler) BalanceHandler(writer http.ResponseWriter, request *http.Request) {
	method := request.Method
	body, err := io.ReadAll(request.Body)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func() {
		err := request.Body.Close()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}()
	id := 0
	balance := 0
	str_err := ""
	if method == "GET" {
		var req model.RequestGet
		err = json.Unmarshal(body, &req)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			writer.Write([]byte("неверный запрос"))
			return
		}
		id, balance, str_err = handler.bank_service.GetBalance(req.UserId)
	} else if method == "PUT" {
		var req model.RequestPut
		err = json.Unmarshal(body, &req)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			writer.Write([]byte("неверный запрос"))
			return
		}
		id, balance, str_err = handler.bank_service.PutMoneyBalance(req.UserId, req.Amount)
	} else {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("неверный запрос"))
		return
	}

	if str_err != "" {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte(str_err))
		return
	}
	type Response struct {
		UserId  int `json:"user_id"`
		Balance int `json:"balance"`
	}
	ans, err := json.Marshal(Response{UserId: id, Balance: balance})
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write(ans)
}

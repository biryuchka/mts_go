package handlers

import (
	bank_service2 "homework3/internal/bank_service"
	"homework3/internal/config"
)

type Handler struct {
	cfg          *config.Config
	bank_service *bank_service2.BankService
}

func NewHandler(config *config.Config, bank_service *bank_service2.BankService) *Handler {
	return &Handler{cfg: config, bank_service: bank_service}
}

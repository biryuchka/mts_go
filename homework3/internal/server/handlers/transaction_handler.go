package handlers

import (
	"encoding/json"
	"homework3/internal/model"
	"io"
	"net/http"
)

func (handler *Handler) TransactionHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Method != "PUT" {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("неверный запрос"))
		return
	}

	body, err := io.ReadAll(request.Body)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func() {
		err := request.Body.Close()
		if err != nil {
			writer.WriteHeader(http.StatusInternalServerError)
		}
	}()
	var transaction model.Transaction
	err = json.Unmarshal(body, &transaction)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("неверный запрос"))
		return
	}
	user_id, user_balance, str_err := handler.bank_service.BankTransaction(transaction.Amount, transaction.From, transaction.To)
	if str_err != "" {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte(str_err))
		return
	}
	ans, err := json.Marshal(model.Response{UserId: user_id, Balance: user_balance})
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	writer.WriteHeader(http.StatusOK)
	_, _ = writer.Write(ans)
}

package server

import (
	"context"
	"fmt"
	bank_service2 "homework3/internal/bank_service"
	"homework3/internal/config"
	"homework3/internal/database"
	"homework3/internal/database/my"
	"homework3/internal/server/handlers"
	"net/http"
)

type Server struct {
	cfg    *config.Config
	server *http.Server
}

func NewServer(cfg *config.Config) *Server {
	var db database.DataBase = my.NewDataBase()
	bank_service := bank_service2.NewBankService(&db)
	addr := fmt.Sprintf(":%d", cfg.Http.Port)
	return &Server{
		cfg: cfg,
		server: &http.Server{
			Addr:    addr,
			Handler: initApi(cfg, bank_service),
		},
	}
}

func (s *Server) Run() {
	go func() {
		err := s.server.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
}

func (s *Server) Stop(ctx context.Context) {
	fmt.Println(s.server.Shutdown(ctx))
}

func initApi(cfg *config.Config, bank_service *bank_service2.BankService) http.Handler {
	mux := http.NewServeMux()
	handler := handlers.NewHandler(cfg, bank_service)
	mux.HandleFunc("/balance", handler.BalanceHandler)
	mux.HandleFunc("/transaction", handler.TransactionHandler)
	return mux
}
